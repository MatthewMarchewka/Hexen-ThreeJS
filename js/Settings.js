Settings = {
        hexagonWalls:6,
        materialWall: new THREE.MeshPhongMaterial({ 
            specular: 0xcccccc,
            shininess: 30,
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/wall.png') ,
        }),
        materialInvisible: new THREE.MeshBasicMaterial({
            specular: 0xcccccc,
            shininess: 30,
            visible: false,
            side: THREE.DoubleSide, 
        }),
        materialFloor: new THREE.MeshPhongMaterial({ 
            specular: 0xcccccc,
            shininess: 0,
            side: THREE.DoubleSide, 
            map: new THREE.TextureLoader().load('gfx/floor.png') ,
        }),
        pi: Math.PI,
        sideLength: 330,
        initContainer: null,
        ally: [],
        enemies: [],
        higlightedAlly: null,
        followersAlly: [],
        doorsWalls: [],
        doorsOpen: false,
        ringGeometry: new THREE.RingGeometry( 10, 50, 32 ),
        ringMaterial: new THREE.MeshBasicMaterial( { color: 0x39ff14, side: THREE.DoubleSide } ),
        playerMesh: null
}



