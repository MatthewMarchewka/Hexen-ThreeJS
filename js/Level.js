function Level() {
        var hexArr = new LevelData().getLevelData().level
        var hexSize = new LevelData().getLevelData().size
        //generowanie tablicy levelu
        level = []
        for(var i=0; i<hexSize; i++){
            level[i] = []
            for(var j=0; j<hexSize; j++)
                level[i][j] = null   
        }
        //uzupełnianie tablicy levelu
        for(var i=0; i<hexArr.length; i++){
            level[ hexArr[i].x ][ hexArr[i].z ] = hexArr[i]
        }
        //zmienianie dirInów wszystkich elementów
        for(var i=0; i<hexSize; i++){                               // i to pion, j to poziom
            for(var j=0; j<hexSize; j++){
                if(level[i][j]){
                    switch(level[i][j].dirOut){
                        case 0:
                            if(i>0 && level[i-1][j])
                                level[i-1][j].dirIn = 3
                            break;
                        case 1:
                            if(level[i][j].z % 2 == 0){  //parzyste czyli górne rzędy
                                if(i>0 && level[i-1][j+1])
                                    level[i-1][j+1].dirIn = 4   
                            }else{
                                if(level[i][j+1])
                                    level[i][j+1].dirIn = 4
                            }
                            break;
                        case 2:
                            if(level[i][j].z % 2 == 0){  //parzyste czyli górne rzędy
                                if(level[i][j+1])
                                    level[i][j+1].dirIn = 5   
                            }else{
                                if(i<(hexSize-1) && level[i+1][j+1])
                                    level[i+1][j+1].dirIn = 5
                            }
                            break;
                        case 3:
                            if(i<(hexSize-1) && level[i+1][j])
                                level[i+1][j].dirIn = 0
                            break;
                        case 4:
                            if(j % 2 == 0){
                                if(level[i][j-1])
                                    level[i][j-1].dirIn = 1   
                            }else{
                                if(i<(hexSize-1) && level[i+1][j-1])
                                    level[i+1][j-1].dirIn = 1
                            }
                            break;
                        case 5:
                            if(level[i][j].z % 2 == 0){  //parzyste czyli górne rzędy
                                if(i>0 && level[i-1][j-1])
                                    level[i-1][j-1].dirIn = 2   
                            }else{
                                if(level[i][j-1])
                                    level[i][j-1].dirIn = 2
                            }
                    }
                }
            }
        }
        //tworzenie hexów
        for(var i=0; i<hexSize; i++){
            for(var j=0; j<hexSize; j++)
                if(level[i][j]){
                    level[i][j] = new Hex(level[i][j]).getHex()
                    if(!(Settings.initContainer))
                        Settings.initContainer = level[i][j]
                }
        }
        return level
    }

    function newAlly(object){
        var ally = new Ally()
        ally.loadModel("models/homer/tris.js", function (data) {
            ally.getPlayerMesh().userData.index =  Settings.ally.length
            Settings.ally.push(ally)

            object.add(data) // data to obiekt kontenera zwrócony z Model.js
         })
    }
