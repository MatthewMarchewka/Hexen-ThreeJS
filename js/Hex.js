function Hex(obj) {

        //hexagon
        var container = new THREE.Object3D()
        var geometryWall = new THREE.BoxGeometry(Settings.sideLength,100,5);
        //jedna ściana hexagonu
        var wall = new THREE.Mesh(geometryWall, Settings.materialWall);
        //dane hexagonu
        var promien = (Settings.sideLength*Math.sqrt(3))/2
        var sequence = [3,2,1,0,5,4]
        //wielkość hexagonu
        var bbox
        var sizeX
        var sizeZ
        
        for (var i = 0; i < Settings.hexagonWalls ; i++) {
            if( i != obj.dirOut && i != obj.dirIn )  //tworzenie pełnej ściany
                var side = wall.clone()
            else                                  //klonowanie wyjścia/wejścia
                var side = new Doors(obj.type).getDoors()
            //ustawianie ścian hexagonu
            side.position.x =  promien * Math.sin(sequence[i]*(Settings.pi/3))
            side.position.z =  promien * Math.cos(sequence[i]*(Settings.pi/3))
            side.lookAt(container.position)
            //dodanie jej do głównego elementu  
            container.add(side)
        }
        bbox = new THREE.Box3().setFromObject(container);
        sizeX = bbox.max.x - bbox.min.x
        sizeZ = bbox.max.z - bbox.min.z
        //tworzenie podłogi
         var geometryFloor = new THREE.BoxGeometry(sizeX, 1, sizeZ)
         var floor = new THREE.Mesh(geometryFloor, Settings.materialFloor)
        floor.position.y = -50
        floor.userData = { type: "floor"};
        container.add(floor)

        this.getHex = function () {
            container.position.x = (sizeX-(Settings.sideLength/2))*obj.z // to jest czerwona linia
            container.position.z = sizeZ*obj.x    //to jest niebieska linia
            if(obj.z % 2 != 0)
                container.position.z+= sizeZ/2
            container.userData.containerType = obj.type 
            return container
        }
    }