function LevelData() {
    var obj =
        {
            "size": "8",
            "level": [{
                "id": "3_2",
                "x": 3,
                "z": 2,
                "dirOut": 1,
                "dirIn": 4,
                "type": "ally"
            },
            {
                "id": "2_3",
                "x": 2,
                "z": 3,
                "dirOut": 1,
                "dirIn": 4,
                "type": "doors"
            },
            {
                "id": "2_4",
                "x": 2,
                "z": 4,
                "dirOut": 3,
                "dirIn": 0,
                "type": "ally"
            },
            {
                "id": "3_4",
                "x": 3,
                "z": 4,
                "dirOut": 3,
                "dirIn": 0,
                "type": "enemy"
            },
            {
                "id": "4_4",
                "x": 4,
                "z": 4,
                "dirOut": 3,
                "dirIn": 0,
                "type": "ally"
            },
            {
                "id": "5_4",
                "x": 5,
                "z": 4,
                "dirOut": 4,
                "dirIn": 1,
                "type": "wall"
            },
            {
                "id": "5_3",
                "x": 5,
                "z": 3,
                "dirOut": 5,
                "dirIn": 2,
                "type": "light"
            },
            {
                "id": "5_2",
                "x": 5,
                "z": 2,
                "dirOut": 5,
                "dirIn": 2,
                "type": "doors"
            },
            {
                "id": "4_1",
                "x": 4,
                "z": 1,
                "dirOut": 0,
                "dirIn": 3,
                "type": "ally"
            },
            {
                "id": "3_1",
                "x": 3,
                "z": 1,
                "dirOut": 1,
                "dirIn": 4,
                "type": "wall"
            }]
        }
    this.getLevelData = function () {
        return obj
    }
}
