function Doors(type) {
    var geometryOpenWall = new THREE.BoxGeometry(Settings.sideLength/3,100,5);
    var openWall = new THREE.Mesh(geometryOpenWall,  Settings.materialWall);

    var doorsContainer = new THREE.Object3D()
    for(var i=0; i<2; i++){
        var doorsSide = openWall.clone()
        doorsSide.position.x = (i*(Settings.sideLength/1.5))-(Settings.sideLength/3)
        doorsContainer.add(doorsSide)
    }

    if(type == "doors"){
        var particles = new THREE.Geometry() // geometria - tablica cząsteczek
        var particleMaterial = new THREE.PointsMaterial(
            {
                color: 0xff3300,
                size: 10, // ta wartośc zmieniamy suwakiem skali
                map: new THREE.TextureLoader().load("gfx/particle.png"),
                blending: THREE.AdditiveBlending,
                transparent: true,
                depthWrite: false,
                opacity: 0.6
            });
        
        var v1 = new THREE.Vector3(-42, -48, 0)
        var v2 = new THREE.Vector3(42, 48, 0)
        var subV = v2.clone().sub(v1.clone())
        var stepV = subV.divideScalar(50)
        var particleFactor = []
        for(var i=0; i<50; i++){
            var particle = new THREE.Vector3(
              v1.x + stepV.x * i,
              v1.y + stepV.y * i,
              v1.z + stepV.z * i) 
            particles.vertices.push(particle);
            particleFactor.push(randomize(0,180))

            var particle = new THREE.Vector3(
                (v1.x*-1) - stepV.x * i,
                v1.y + stepV.y * i,
                v1.z + stepV.z * i) 
              particles.vertices.push(particle);
              particleFactor.push(randomize(0,180))
         }
         var particleSystem = new THREE.Points(particles, particleMaterial);
         doorsContainer.add(particleSystem)

         var invisibleWall = new THREE.Mesh(geometryOpenWall,  Settings.materialInvisible);
         invisibleWall.userData.invisible = "true"
         doorsContainer.add(invisibleWall)

         Settings.doorsWalls.push({wall: invisibleWall, particleSystem: particleSystem, particles: particles, direction: -3, particleFactor: particleFactor})
        }

    
    

    this.getDoors = function () {
        return doorsContainer
    }
}