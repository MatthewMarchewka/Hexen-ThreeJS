function Enemy() {

    var container = new THREE.Object3D
    var mixer
    var meshModel
    var rayLine
    var newPosition
    var endPrompt

    var way = Math.floor(Settings.sideLength*Math.sqrt(3))-33

    this.direction = -2
    this.rotation = 270 * Math.PI/180
    this.lookAround = 0

    var modelMaterial = new THREE.MeshBasicMaterial({
        map: THREE.ImageUtils.loadTexture("models/bobo/bobo.png"),
        morphTargets: true // odpowiada za animację materiału modelu
    });

    this.loadModel = function (url, callback) {
        var loader = new THREE.JSONLoader();
        loader.load(url, function (geometry) {
            meshModel = new THREE.Mesh(geometry, modelMaterial)
            meshModel.name = "Bobo";
            meshModel.userData = { type: "bobo"};
            var box = new THREE.Box3().setFromObject(meshModel);
            meshModel.position.y = (box.getSize().y); // ustaw pozycje modelu
            meshModel.scale.set(2 , 2 , 2); // ustaw skalę modelu
            mixer = new THREE.AnimationMixer(meshModel);
            mixer.clipAction("duktrot").play();
            //dodanie modelu do kontenera
            container.add(meshModel)
            var light = new THREE.SpotLight(0xfff1e0, 0.5, 300, Settings.pi);
            light.position.y = 160
            light.position.z -= 120
            light.position.x -= 20
            container.add(light)
            container.position.y = -50
            container.rotation.y = 270 * Math.PI/180
            meshModel.rotation.y = Math.PI/2
            //pole na podłodze
            var geometry = new THREE.RingGeometry(0, 100, 32, 8, (Math.PI/180)*45, Math.PI/2 );
            var mesh = new THREE.Mesh( geometry, Settings.ringMaterial );
            mesh.rotateX(Settings.pi/2)
            mesh.position.y = 1
            mesh.position.z = 15
            container.add( mesh );
            //kreska na podłodze
            var lineMaterial = new THREE.LineBasicMaterial({ color: 0xff0000 });
            var lineGeometry = new THREE.Geometry();
            lineGeometry.vertices.push(new THREE.Vector3(0, 0, 15));
            newPosition = new THREE.Vector3( 
                0,
                0,
                115,
            )
            lineGeometry.vertices.push(newPosition);
            rayLine = new THREE.Line(lineGeometry, lineMaterial);
            rayLine.position.y = 2
            rayLine.rotation.y = 44 * Math.PI/180
            container.add(rayLine)
            callback(container);

        });
    }

   // update mixera    
    this.updateEnemy = function (delta) {
        //console.log(delta)
        if (mixer) mixer.update(delta)
    }

   //animowanie postaci
    this.setRun = function () {
        mixer.clipAction("run").play();
    }
    this.setStand = function () {
        mixer.uncacheRoot(meshModel)
        mixer.clipAction("stand").play();
    }

    //funkcja zwracająca kontener
    this.getPlayerCont = function () {
        return container
    }

    var lineRotate = 44
    var rotateDestination = -1
    var myRay
    var origin
    var raycaster = new THREE.Raycaster(); // obiekt symulujący "rzucanie" promieni

    function odleglosc(x1, z1, x2, z2){
        return Math.sqrt(Math.pow(x2-x1,2)*Math.pow(z2-z1,2))
    }

    function endGame(){
        if(!endPrompt){
            endPrompt = $('<div>',{class: "endPrompt"}).html("<br><h1>Zebrałeś: <b>"+Settings.followersAlly.length+" pomocników</b></h1><br><br><button onclick='location.reload()'>Refresh website for new game</button>")
            $("body").append(endPrompt)
        }
    }

    this.move = function (arr) {
        lineRotate+= rotateDestination
        if(way >= 123 && way <= 125){
            this.lookAround++
        }else if((way <= Math.floor(Settings.sideLength*Math.sqrt(3))-70) && (way >= Math.floor(Settings.sideLength*Math.sqrt(3))-72)){
            this.lookAround++
        }
        if(this.lookAround > 0){
            this.direction = 0
            this.rotation = (60*Math.sin(5* this.lookAround * Math.PI/180)) * Math.PI/180
            if(this.lookAround == 80){
                this.lookAround = 0
                if(way < 126){
                    this.direction = 2
                    this.rotation = 90 * Math.PI/180
                }else{
                    this.direction = -2
                    this.rotation = 270 * Math.PI/180
                }
            }
        }
        if(lineRotate == -44){
            rotateDestination = 1
        }else if(lineRotate == 44){
            rotateDestination = -1
        }
        container.rotation.y = this.rotation
        container.position.x+= this.direction
        way+= this.direction
        rayLine.rotation.y = lineRotate * Math.PI/180
        raycaster.set(container.position.clone().add(new THREE.Vector3(0,70,0)), meshModel.getWorldDirection().clone().applyAxisAngle(new THREE.Vector3(0,1,0),(lineRotate-90) * Math.PI/180))
        var inter = raycaster.intersectObjects(arr)
        //console.log(Settings.playerMesh)
        if (inter[0]) {
            if(inter[0].object.userData.type == "invisible" && inter[0].distance < 115){
                endGame()
            }             
        }
    }


    //funkcja zwracająca playera
    this.getPlayerMesh = function () {
        return meshModel
    }
}