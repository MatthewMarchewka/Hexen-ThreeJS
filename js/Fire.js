function Fire() {
    var container = new THREE.Object3D()
    var geometry = new THREE.BoxGeometry(4, 4, 4);
    var material = new THREE.MeshBasicMaterial({
        color: 0xff6600,
        transparent: true,
        opacity: 0.5,
        depthWrite: false,
        blending: THREE.AdditiveBlending // kluczowy element zapewniający mieszanie kolorów poszczególnych cząsteczek
    });

    var particles = []
    var particle = new THREE.Mesh(geometry, material.clone())

    for(var i=0; i<50; i++){
        var mesh = particle.clone()
        var scale = randomize(1,5)
        mesh.position.x = randomize(-35,35)
        mesh.position.z = randomize(-35,35)
        mesh.userData.moveSpeed = randomize(1,3)
        mesh.scale.set(scale,scale,scale)
        particles.push(mesh)
        container.add(mesh)
    }

    var light = new THREE.PointLight( 0xff6600, 5, 160 );
    light.position.y = 130
    container.add(light)
    container.position.y = -50


    //funkcja zwracająca kontener
    this.getFire = function () {
        return container
    }
    //funkcja aktualizująca cząsteczki ognia
    this.update = function () {
        for(var i=0; i<particles.length; i++){
            if(particles[i].position.y > 120){
                particles[i].position.y = randomize(0,5)
                particles[i].opacity = 1
            }
            particles[i].opacity-= 0.01
            particles[i].position.y+= particles[i].userData.moveSpeed
        }
    }

}