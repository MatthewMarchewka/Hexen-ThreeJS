function Ally() {
        this.follow = false
        this.followedEl = null
    
        var container = new THREE.Object3D
        var mixer
        var meshModel
        var ring

        var modelMaterial = new THREE.MeshBasicMaterial({
            map: THREE.ImageUtils.loadTexture("models/homer/homer.png"),
            morphTargets: true // odpowiada za animację materiału modelu
        });
    
        this.loadModel = function (url, callback) {
            var loader = new THREE.JSONLoader();
            loader.load(url, function (geometry) {
                meshModel = new THREE.Mesh(geometry, modelMaterial)
                meshModel.name = "Homer";
                meshModel.userData = { type: "ally"};
                var box = new THREE.Box3().setFromObject(meshModel);
                meshModel.position.y = (box.getSize().y); // ustaw pozycje modelu
                meshModel.scale.set(2 , 2 , 2); // ustaw skalę modelu
                mixer = new THREE.AnimationMixer(meshModel);
                mixer.clipAction("stand").play();
                //dodanie modelu do kontenera
                container.add(meshModel)
                var light = new THREE.SpotLight(0xfff1e0, 0.5, 300, Settings.pi);
                light.position.y = 160
                light.position.z -= 120
                light.position.x -= 20
                container.add(light)
                // zwrócenie kontenera
                container.position.y = -50
                callback(container);

            });
        }
 
       // update mixera    
        this.updateAlly = function (delta) {
            //console.log(delta)
            if (mixer) mixer.update(delta)
        }
    
       //animowanie postaci
        this.setRun = function () {
            mixer.clipAction("run").play();
        }
        this.setStand = function () {
            mixer.uncacheRoot(meshModel)
            mixer.clipAction("stand").play();
        }

        //wybieranie postaci najechaniem myszki
        this.setRing = function () {
            if(!ring){
                ring = new THREE.Mesh( Settings.ringGeometry, Settings.ringMaterial );
                ring.rotateX(Settings.pi/2)
                ring.position.y = 1
                container.add(ring)
            }
        }
        this.removeRing = function () {
            container.remove(ring)
            ring = null
        }
        //funkcja zwracająca kontener
        this.getPlayerCont = function () {
            return container
        }

        //funkcja zwracająca playera
        this.getPlayerMesh = function () {
            return meshModel
        }
    }