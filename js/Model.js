function Model() {
    
        var container = new THREE.Object3D
        var mixer
        var meshModel
        var wall

        var modelMaterial = new THREE.MeshBasicMaterial({
            map: THREE.ImageUtils.loadTexture("models/vader/vader.png"),
            morphTargets: true // odpowiada za animację materiału modelu
        });
    
        this.loadModel = function (url, callback) {
            var loader = new THREE.JSONLoader();
            loader.load(url, function (geometry) {
                meshModel = new THREE.Mesh(geometry, modelMaterial)
                meshModel.name = "Vader";
                var box = new THREE.Box3().setFromObject(meshModel);
                meshModel.position.y = (box.getSize().y); // ustaw pozycje modelu
                meshModel.scale.set(1.5 , 1.5 , 1.5); // ustaw skalę modelu
                mixer = new THREE.AnimationMixer(meshModel);
                mixer.clipAction("Stand").play();
                //dodanie modelu do kontenera
                container.add(meshModel)
                var light = new THREE.SpotLight(0xfff1e0, 1, 300, Settings.pi);
                light.position.y = 160
                light.position.z -= 120
                light.position.x -= 20
                container.add(light)
                var geometryWall = new THREE.BoxGeometry(box.getSize().x*1.5,box.getSize().y*3,box.getSize().z*1.5);
                wall = new THREE.Mesh(geometryWall,  Settings.materialInvisible);
                wall.userData.type = "invisible"
                container.add(wall)
                callback(container); 
            });
        }
 
       // update mixera    
        this.updateModel = function (delta) {
            // console.log(delta)
            if (mixer) mixer.update(delta)
        }
    
       //animowanie postaci
        this.setRun = function () {
            mixer.clipAction("Run").play();
        }
        this.setStand = function () {
            mixer.uncacheRoot(meshModel)
            mixer.clipAction("Stand").play();
        }
        //funkcja zwracająca kontener
        this.getPlayerCont = function () {
            return container
        }

        //funkcja zwracająca playera
        this.getPlayerMesh = function () {
            return meshModel
        }
        this.getPlayerWall = function () {
            return wall
        }
    }