//************************* METODY *************************

//************************* FUNKCJE *************************
function randomize(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min
}

$(document).ready(function(){
    scene = new THREE.Scene();
    var camera = new THREE.PerspectiveCamera(
        45, // kąt patrzenia kamery (FOV - field of view)
        $(window).width()/$(window).height(), // proporcje widoku, powinny odpowiadać proporjom naszego ekranu przeglądarki
        0.1, // minimalna renderowana odległość
        10000 // maxymalna renderowana odległość
        );
    var renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x000000);   //ustalanie koloru tła sceny
    renderer.setSize($(window).width(), $(window).height() );         //wielkość okna
    $("#root").append( renderer.domElement );
    camera.position.set(200,300,900)
    var cameraY = camera.position.y
    camera.fov = 90;
    camera.updateProjectionMatrix();
    camera.lookAt(500,0,500)
    var orbitControl = new THREE.OrbitControls(camera, renderer.domElement);
    orbitControl.addEventListener('change', function () {
        renderer.render(scene, camera)
    });
//************************* LINIE PERSPEKTYWY *************************
    //dodajemy linie perspektywy
    var axes = new THREE.AxesHelper(1000)
    scene.add(axes)
    var raycaster = new THREE.Raycaster(); // obiekt symulujący "rzucanie" promieni
    var mouseVector = new THREE.Vector2() // ten wektor czyli pozycja w przestrzeni 2D na ekranie(x,y) wykorzystany będzie do określenie pozycji myszy na ekranie a potem przeliczenia na pozycje 3D
//************************* LISTENERS *************************
    var directionVect = null
    $(document).keydown(function(event){
        if(event.key == " "){
            if(Settings.doorsOpen){
                for(var i=0; i<Settings.doorsWalls.length; i++){
                    meshArray.push(Settings.doorsWalls[i].wall)
                    Settings.doorsWalls[i].particleSystem.geometry.verticesNeedUpdate = true; // ta funkcja wymusza zmiany w systemie cząsteczek
                    Settings.doorsWalls[i].particleSystem.material.size = 10 // zmiana skali wszystkich cząsteczek
                }
                Settings.doorsOpen = false
            }else{
                console.log("XXXX")
                for(var i=meshArray.length-1; i>-1; i--){
                    if(meshArray[i].userData.invisible == "true"){
                        meshArray.splice(i,1)
                    }
                }
                for(var i=0; i<Settings.doorsWalls.length; i++){
                    Settings.doorsWalls[i].particleSystem.geometry.verticesNeedUpdate = true; // ta funkcja wymusza zmiany w systemie cząsteczek
                    Settings.doorsWalls[i].particleSystem.material.size = 0 // zmiana skali wszystkich cząsteczek
                }
                Settings.doorsOpen = true
            }
        }
    })
    $("#root").mousedown(function (event) {
        mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
        mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
        raycaster.setFromCamera(mouseVector, camera);
        var intersects = raycaster.intersectObjects(scene.children, true);
        if (intersects.length > 0) {
            if(searchCorrect("ally", intersects) != -1){        //kliknięcie ally
                var allyIndex = searchCorrect("ally", intersects)
                var clickedAlly = Settings.ally[ intersects[allyIndex].object.userData.index ]
                for(var i=meshArray.length-1; i>-1; i--){
                    if(meshArray[i].userData.invisible == "true"){
                        meshArray.splice(i,1)
                    }
                }
                for(var i=0; i<Settings.doorsWalls.length; i++){
                    Settings.doorsWalls[i].particleSystem.geometry.verticesNeedUpdate = true; // ta funkcja wymusza zmiany w systemie cząsteczek
                    Settings.doorsWalls[i].particleSystem.material.size = 0 // zmiana skali wszystkich cząsteczek
                }
                Settings.doorsOpen = true
                if(!clickedAlly.follow){
                    if(Settings.followersAlly.length != 0)
                        clickedAlly.followedEl = Settings.followersAlly[ Settings.followersAlly.length - 1]
                    else
                        clickedAlly.followedEl = model
                    clickedAlly.follow = true
                    Settings.followersAlly.push(clickedAlly)
                }
            }else if(searchCorrect("floor", intersects) != -1){ //klikniecie podłogi
                clickedVect = intersects[searchCorrect("floor", intersects)].point   //kliknięte miejsce
                directionVect = clickedVect.clone().sub(model.getPlayerCont().position).normalize()    //kliknięte miejsce w współrzędnych do 1
                //kąt obrócenia playera do klikniętego miejsca
                var angle = Math.atan2(
                    clickedVect.x - model.getPlayerCont().position.clone().x,
                    clickedVect.z - model.getPlayerCont().position.clone().z
                )
                model.getPlayerMesh().rotation.y = angle
            }
        }
    })

    $("#root").mousemove(function (event) {
        mouseVector.x = (event.clientX / $(window).width()) * 2 - 1;
        mouseVector.y = -(event.clientY / $(window).height()) * 2 + 1;
        raycaster.setFromCamera(mouseVector, camera);
        var intersects = raycaster.intersectObjects(scene.children, true);
        if (intersects.length > 0) {
            var allyIndex = searchCorrect("ally", intersects)
            if(allyIndex != -1){        //kliknięcie ally
                Settings.higlightedAlly = Settings.ally[ intersects[allyIndex].object.userData.index ]
                Settings.higlightedAlly.setRing()
            }else{
                if(Settings.higlightedAlly){
                    Settings.higlightedAlly.removeRing()
                    Settings.higlightedAlly = null
                }
            }
        }
    })

    function searchCorrect(type, intersects){
            for(var i=0; i<intersects.length; i++){
                if(intersects[i].object.userData.type == type)
                    return i
            }
            return -1
    }
//************************* GENEROWANIE LEVELU *************************
    var arrFire = []
    var objectsArr = Level()
    var meshArray = []

    for(var i=0; i<objectsArr.length; i++){
        for(var j=0; j<objectsArr[i].length; j++){
            if(objectsArr[i][j]){
                scene.add(objectsArr[i][j])
                if(objectsArr[i][j].userData.containerType == "ally"){
                    createAlly(objectsArr[i][j].position)
                }else if(objectsArr[i][j].userData.containerType == "enemy"){
                    createEnemy(objectsArr[i][j].position, objectsArr[i][j])
                }else if(objectsArr[i][j].userData.containerType == "light"){
                    var fire = new Fire()
                    fire.getFire().position.x = objectsArr[i][j].position.x
                    fire.getFire().position.z = objectsArr[i][j].position.z
                    arrFire.push(fire)
                    scene.add(fire.getFire()) 
                }else if(objectsArr[i][j].userData.containerType == "doors"){
                }
                meshArrayFill(objectsArr[i][j])
            }
        }
    }

    function meshArrayFill(obj){
        for(var i=0; i<obj.children.length;i++){
            if(obj.children[i].userData.type != "floor" && obj.children[i].type == "Mesh"){
                meshArray.push(obj.children[i])
            }else if(obj.children[i].type == "Object3D"){
                for(var j=0; j< obj.children[i].children.length; j++)
                     meshArray.push(obj.children[i].children[j])
            }
        }
    }

    function createAlly(allyPosition){
            var ally = new Ally()
            ally.loadModel("models/homer/tris.js", function (data) {
                ally.getPlayerMesh().userData.index =  Settings.ally.length
                data.position.x = allyPosition.x
                data.position.z = allyPosition.z
                Settings.ally.push(ally)
                scene.add(data)
            })
    }

    function createEnemy(enemyPosition, object){
        var enemy = new Enemy()
        enemy.loadModel("models/bobo/tris.js", function (data) {
            data.position.x = (enemyPosition.x  + (Settings.sideLength*Math.sqrt(3))/2) - 75
            data.position.z = enemyPosition.z
            Settings.enemies.push(enemy)
            scene.add(data)
        })
    }
    var lightSphereMaterial = new THREE.MeshBasicMaterial({         //ustalanie materiału z którego stworzona jest kulas
        wireframe: true,
        transparent: true,
        opacity: 0.5,
        color: 0xff0000,
        side: THREE.DoubleSide
    })
    var lightSphereGeometry = new THREE.SphereGeometry(10, 8, 8);   // ustalanie geometrii światła
    var light = null
//************************* DODANIE USERA DO SCENY *************************
    var model = new Model()
    model.loadModel("models/vader/tris.js", function (data) {
        model.getPlayerCont().position.x = Settings.initContainer.position.x
        model.getPlayerCont().position.y = -49
        model.getPlayerCont().position.z = Settings.initContainer.position.z
        scene.add(data) // data to obiekt kontenera zwrócony z Model.js
        meshArray.push(model.getPlayerWall())
        Settings.playerMesh = model.getPlayerCont()
     })
//************************* DODANIE LICZNIKA FPS DO SCENY *************************
     var stats = new Stats();
     stats.showPanel(0);
     document.body.appendChild(stats.dom);

//************************* FUNKCJA RENDER *************************   
    //render variables
    var clock = new THREE.Clock();
    var clickedVectAlly
    var allyDirectionVect
    var allyAngle
    var originPoint
    var ray
    //render function
    function render() {
        stats.begin();
        //update ognia
        for(var i=0; i< arrFire.length; i++)
            arrFire[i].update()
        //kamera podąża za playerem
        camera.position.x = model.getPlayerCont().position.x
        camera.position.z = model.getPlayerCont().position.z + 200
        camera.position.y = model.getPlayerCont().position.y + 200
        camera.lookAt(model.getPlayerCont().position)
        //animowanie drzwi
        for(var i=0; i<Settings.doorsWalls.length; i++){
            var verts = Settings.doorsWalls[i].particles.vertices
            Settings.doorsWalls[i].direction++
            if(Settings.doorsWalls[i].direction == 4)
                Settings.doorsWalls[i].direction = -3
            for (var j = 0; j < verts.length; j++) {
                var particle = verts[j];
                 particle.x += Settings.doorsWalls[i].direction * Math.sin(Settings.doorsWalls[i].particleFactor[j])
                 particle.y += Settings.doorsWalls[i].direction * Math.sin(Settings.doorsWalls[i].particleFactor[j])
                 particle.z += Settings.doorsWalls[i].direction * Math.sin(Settings.doorsWalls[i].particleFactor[j])   
            }
            Settings.doorsWalls[i].particleSystem.geometry.verticesNeedUpdate = true
        }
        //animowanie głównego modelu
        delta = clock.getDelta()
        model.updateModel(delta)
        //animowanie pomocników
        for(var i=0; i<Settings.ally.length; i++){
            Settings.ally[i].updateAlly(delta)
            if(Settings.ally[i].follow){
                clickedVectAlly = Settings.ally[i].followedEl.getPlayerCont().position
                allyDirectionVect = clickedVectAlly.clone().sub(Settings.ally[i].getPlayerCont().position).normalize() 
                if(Settings.ally[i].getPlayerCont().position.clone().distanceTo(clickedVectAlly) > 50){
                    Settings.ally[i].setRun()
                    Settings.ally[i].getPlayerCont().translateOnAxis(allyDirectionVect, 3)
                    allyAngle = Math.atan2(
                        Settings.ally[i].getPlayerCont().position.clone().x - clickedVectAlly.x,
                        Settings.ally[i].getPlayerCont().position.clone().z - clickedVectAlly.z
                    )
                    Settings.ally[i].getPlayerMesh().rotation.y = allyAngle - 2
                }else{
                    Settings.ally[i].setStand()
                    allyDirectionVect = null
                }
            }
        }
        //animowanie przeciwników
        for(var i=0; i<Settings.enemies.length; i++){
            Settings.enemies[i].updateEnemy(delta)
            Settings.enemies[i].move(meshArray)

        }
        //przesuwanie głównym modelem
        if(directionVect){      //warunek do podążania do elementu
            model.setRun()
            model.getPlayerCont().translateOnAxis(directionVect, 3)
            originPoint = model.getPlayerCont().position.clone();
            ray = new THREE.Ray(originPoint, model.getPlayerMesh().getWorldDirection().clone())
            raycaster.ray = ray
            var intersects = raycaster.intersectObjects(meshArray, true);
            if (intersects[0]) {
                if(intersects[0].object.userData.type != "invisible"){
                    if(intersects[0].distance < 30){ // odległość od vertex-a na wprost, zgodnie z kierunkiem ruchu
                        directionVect = null
                        model.setStand()
                    }                  
                    if(light)
                        scene.remove(light)
                    light = new THREE.Mesh(lightSphereGeometry, lightSphereMaterial); //tworzenie widzialnego obiektu światła
                    light.position.x = intersects[0].point.x
                    light.position.y = intersects[0].point.y
                    light.position.z = intersects[0].point.z
                    scene.add(light);
                }
            }
            if(model.getPlayerCont().position.clone().distanceTo(clickedVect) < 3){
                directionVect = null
                model.setStand()
            }
        }

//************************* KONIEC RENDER *************************  
        requestAnimationFrame(render);
        //ciągłe renderowanie / wyświetlanie widoku sceny nasza kamerą
        renderer.render(scene, camera);
        stats.end();
    }
    render();
})