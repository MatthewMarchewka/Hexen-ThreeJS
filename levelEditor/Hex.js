function _Hex(id, x, z, dirOut, dirIn, type, condition) {
    //hexen
    this.htmlObject = null
    this.id = id
    this.x = x
    this.z = z
    this.dirOut = dirOut
    this.dirIn = dirIn
    this.type = type  // to będzie zależne od zaznaczenia
    this.jsonArrIndex = -1
    this.content = null
    //strzłka do wskazywania
    this.arrowObject = null
    this.rotationSpan = null

    this.init = function(){
            var hexDiv = $("<div>").addClass("hex").css({top: this.x*64, left: this.z*64}).attr("id",this.id);
            if(this.z % 2 != 0)
                hexDiv.css('transform','translateY(32px)');
            function focusFunction(object, clickInfo) {
                if(mode == "wall"){
                    if(object.arrowObject == null){     // nie klikany wcześniej element (tworzenie wszystkiego)
                        object.arrowObject = $('<img>',{alt: "arrow",src:'../gfx/arrow.png'})
                        object.htmlObject.append(object.arrowObject)
                        object.rotationSpan = $('<span>',{class: "rotationSpan"}).html(object.dirOut)
                        object.htmlObject.append(object.rotationSpan)
                        object.jsonArrIndex = mapJson.level.length
                        mapJson.level.push({id: object.id, x: object.x, z: object.z ,dirOut: object.dirOut, dirIn: object.dirIn, type: object.type})
                        $("#generatedJson").val(JSON.stringify(mapJson).replace(/,/g,",\n"))
                    }else{                             //dodanie cyfry do istniejącego obrotu
                        object.arrowObject.css("transform", "rotate("+(object.dirOut*60)+"deg)")
                        object.rotationSpan.html(object.dirOut)
                        mapJson.level[object.jsonArrIndex].dirOut = object.dirOut
                        mapJson.level[object.jsonArrIndex].dirIn = object.dirIn
                        $("#generatedJson").val(JSON.stringify(mapJson).replace(/,/g,",\n"))
                    }
                    if(object.dirOut == 5){
                        object.dirOut = 0
                        object.dirIn = 3
                    }
                    else
                        object.dirOut++
                    object.dirIn = (object.dirOut + 3) % 6
                }else if(mode != "wall" && object.arrowObject){
                        if(object.content)
                            object.content.remove()
                        object.content = $('<img>',{alt: "add",src:'../gfx/'+mode+'.png'})
                        object.htmlObject.append(object.content)
                        object.type = mode
                        mapJson.level[object.jsonArrIndex].type = object.type
                        $("#generatedJson").val(JSON.stringify(mapJson).replace(/,/g,",\n"))
                }
            }
            hexDiv.on("click", focusFunction.bind(null, this))
            this.htmlObject = hexDiv
            $("#previewDiv").append(hexDiv)
    }
    this.arrow = function(){
        this.arrowObject = $('<img>',{alt: "arrow",src:'../gfx/arrow.png'})
        this.htmlObject.append(this.arrowObject)
        this.arrowObject.css("transform", "rotate("+(this.dirOut*60)+"deg)")
        this.rotationSpan = $('<span>',{class: "rotationSpan"}).html(this.dirOut)
        this.htmlObject.append(this.rotationSpan)
        this.jsonArrIndex = mapJson.level.length
        mapJson.level.push({id: this.id, x: this.x, z: this.z ,dirOut: this.dirOut, dirIn: this.dirIn, type: this.type})
        $("#generatedJson").val(JSON.stringify(mapJson).replace(/,/g,",\n"))
        if(this.dirOut == 5){
            this.dirOut = 0
            this.dirIn = 3
        }
        else
            this.dirOut++
        this.dirIn = (this.dirOut + 3) % 6
        if(this.type == "ally"){
            this.content = $('<img>',{alt: "arrow",src:'../gfx/ally.png'})
            this.htmlObject.append(this.content)
        }
    }
    this.init()
    if(condition)
        this.arrow()



}