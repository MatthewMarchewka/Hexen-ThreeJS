// zmienne
var functionsArr = ["wall","enemy","treasure","light", "ally"]
var hexSize = 64
mapJson = {
    size: 0,
    level: []
}
mode = "wall"

//funkcje
function configFill(){
    for(var i=0; i<functionsArr.length;i++){
        var functionButton = $("<button>").html(functionsArr[i])
        functionButton.on("click", function(){
            mode = this.innerHTML
        })
        $("#config").append(functionButton)
    }
}

function previewFill(){
    var previewDiv = $("<div>").attr('id', 'previewDiv')
    $("#preview").append(previewDiv)
    gridGenerate( $("#grid").val(), false )
}

function jsonPreviewFill(){
    var generatedJson = $("<textarea>").attr('id', 'generatedJson')
    $("#jsonPreview").append(generatedJson)
    var clipboardButton = $("<button>").attr('id', 'clipboard').html("Select text")
    clipboardButton.on("click", function(){
        $("#generatedJson").focus()
        $("#generatedJson").select()
    })
    $("#jsonPreview").append(clipboardButton)
    var generateButton = $("<button>").attr('id', 'generate').html("Generate map")
    generateButton.on("click", function(){
        var generateField = JSON.parse($("#generatedJson").val())
        $("#generatedJson").val('');
        mapJson.size = generateField.size
        gridGenerate(generateField.size, true)
        for(var i=0; i<generateField.level.length; i++){
            $("#"+generateField.level[i].id).remove()
            new _Hex(generateField.level[i].id, generateField.level[i].x, generateField.level[i].z, generateField.level[i].dirOut, generateField.level[i].dirIn, generateField.level[i].type, true)
        }
    })
    $("#jsonPreview").append(generateButton)
}

function gridGenerate( size, conditionClear ){
    //czysczenie diva
    mapJson.size = size
    mapJson.level = []
    if(conditionClear)
        $("#generatedJson").empty()
    $("#previewDiv").empty()
    $('select option[value="'+size+'"]').attr("selected",true);
    //tworzenie nowej siatki
    for(var i=0; i<size; i++)
        for(var j=0; j<size; j++)
            new _Hex(i+"_"+j, i, j, 0, 3, "wall", false)
}
document.addEventListener("DOMContentLoaded", function(event){
    configFill()
    previewFill()
    jsonPreviewFill()

    $("#previewDiv").on("mousedown",function(e) {
        e.preventDefault()
    });
    $( "#grid" ).on("change",function() {
        gridGenerate($( "#grid" ).val(), true)
    });
})